class PagesController < ApplicationController
  rescue_from Exception, with: :exception

  def index
    @gifs = Gif.all
  end

  def create
    ilist = Magick::ImageList.new
    gif_params[:files].each do |i|
      ilist.read i.path
    end
    ilist.each do |image|
      image = image.resize_to_fill!(gif_params[:width].to_i, gif_params[:height].to_i)
    end
    ilist.iterations = 0
    ilist.delay = gif_params[:delay]
    ilist.ticks_per_second = gif_params[:ticks_per_second]

    file = Tempfile.new
    file.binmode
    file.write ilist.to_blob { |attrs| attrs.format = 'gif' }
    file.open

    gif = Gif.create(name: gif_params[:name])
    gif.image.attach(io: file, filename: "#{gif_params[:name]}.gif")

    file.close(unlink_now: true)

    redirect_to root_path
  end

  def edit_gif
    code = rand(36**15).to_s(36)
    @images_blob = []

    @gif = Gif.find(params[:id])

    ilist = Magick::ImageList.new(ActiveStorage::Blob.service.send(:path_for, @gif.image.key))
    images = ilist.to_a

    images.each_with_index do |image, i|
      file = Tempfile.new
      file.binmode
      file.write image.to_blob
      file.open
      blob = ActiveStorage::Blob.create_after_upload!(io: file, filename: "#{code}-#{i}.jpg")
      file.close(unlink_now: true)

      @images_blob << blob
    end

    @name = @gif.name
    @delay = ilist.delay
    @ticks_per_second = ilist.ticks_per_second
    @width = ilist.first.columns
    @height = ilist.first.rows

    render 'edit'
  end

  def edit_video_gif
    code = rand(36**15).to_s(36)
    @images_blob = []

    video = FFMPEG::Movie.new(gif_params[:file].path.to_s)
    video.screenshot("tmp/#{code}-%d.jpg", { vframes: 999999, frame_rate: gif_params[:fps], resolution: "#{gif_params[:width]}x#{gif_params[:height]}"}, validate: false)
    Dir["tmp/#{code}-*.jpg"].count.times do |i|
      path = Rails.root.join("tmp","#{code}-#{i+1}.jpg")
      file = File.open(path, "rb")
      blob = ActiveStorage::Blob.create_after_upload!(io: file, filename: "#{code}-#{i}.jpg")
      File.delete(path)

      @images_blob << blob
    end

    @name = gif_params[:name]
    @delay = gif_params[:delay]
    @ticks_per_second = gif_params[:ticks_per_second]
    @width = gif_params[:width]
    @height = gif_params[:height]

    render 'edit'
  end

  def update
    image_hash = []
    count = 0
    sort = 0
    gif_params[:individual_images].each do |e|
      if !e[0].include?("index-")
        gif_params[:individual_images].each do |f|
          sort = f[1] if f[0].include?(e[0]) && f[0].include?("index-")
        end
        image_hash << {image: e[0], checked: e[1], sort: sort}
        count += 1 if e[1] == "1"
      end
    end
    if count <= 0
      redirect_to root_path
      return
    end
    image_hash = image_hash.sort_by { |h| h[:sort] }

    ilist = Magick::ImageList.new
    image_hash.each do |image|
      ilist.read ActiveStorage::Blob.service.send(:path_for, image[:image]) if image[:checked] == "1"
      ActiveStorage::Blob.find_by(key: image[:image]).purge
    end

    ilist.each do |image|
      image = image.resize_to_fill!(gif_params[:width].to_i, gif_params[:height].to_i)
    end
    ilist.iterations = 0
    ilist.delay = gif_params[:delay]
    ilist.ticks_per_second = gif_params[:ticks_per_second]

    file = Tempfile.new
    file.binmode
    file.write ilist.to_blob { |attrs| attrs.format = 'gif' }
    file.open

    gif_params[:id].blank? ? @gif = Gif.new : @gif = Gif.find(gif_params[:id])
    @gif.update(name: gif_params[:name])
    @gif.image.purge if @gif.image.attached?
    @gif.image.attach(io: file, filename: "#{gif_params[:name]}.gif")
    @gif.save

    file.close(unlink_now: true)

    redirect_to root_path
  end

  def delete
    Gif.find(params[:id]) do |gif|
      gif.image.purge
      gif.destroy
    end

    redirect_to root_path
  end

  private
  def gif_params
    params.require(:gif).permit(:id, :name, {files: []}, {individual_images: {}}, :delay, :ticks_per_second, :width, :height, :file, :fps)
  end

  def exception
    redirect_to root_path
  end
end
