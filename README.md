# README

This is just a repo to test/figure out ways to create or edit a gif file.

In details:

* Creating a gif with gif or other types of image file

* Deleting the gif files

* Editing the gif file by sorting, or removing individual images, setting the speed or size of the gif

* Converting video files to gif by setting fps, letting it to be able to be edited as well
