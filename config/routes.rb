Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "pages#index"

  post '#', to: 'pages#create', as: 'new_gif'
  delete 'delete/:id', to: 'pages#delete', as: 'delete_gif'
  get 'edit/:id', to: 'pages#edit_gif', as: 'edit_gif'
  post 'edit', to: 'pages#edit_video_gif', as: 'edit_video_gif'
  post 'update', to: 'pages#update', as: 'update_gif'

end
